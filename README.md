# Timeline Widget

This is an Elm implementation of a timeline widget enabling the description of time-blocks in a day.

### Building
The elm widget can be built using the following command in the top level directory.

```sh
elm make src/Main.elm --output=timeline.js
```

Then, the `timeline-demo.html` file in the same directory provides all the information you need as to how to include this widget into your existing project.

### Server
In order to simulate the server API, there is a simple demo Flask server. The required packages can be installed using the following command:

```sh
pip install -r requirements.txt
```

Then the server is started using a following command:
```sh
python server.py
```

It starts a basic server on the address `http://localhost:5000` that serves `JSON` strings according to the required sepcification on the `/structure` and `/data` addresses. 
The `/data` also accepts POST requests using the same `JSON` specification.

The default strings used on a startup are stored in variables in `data.py` file.


#### Note:
This build is still under development, some functionality is restricted.
