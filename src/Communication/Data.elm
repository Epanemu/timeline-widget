module Communication.Data exposing (dataDecoder, dataEncoder, loadToTimeline, timelineToData)

import Blocks
import Common exposing (Block, BlockData, BlockID, BlockLabel(..), DataWrapper, Delimiter, Tab, TabData, Time, TimeFormValues, TimeRange, Timeline, timeToString, timeToYPos)
import Communication.Structure exposing (timeDecoder)
import Delimiters
import Dict exposing (Dict)
import Json.Decode as Decode
import Json.Encode as Encode
import Set


{-| JSON decoder for DataWrapper
-}
dataDecoder : Decode.Decoder DataWrapper
dataDecoder =
    Decode.map3 DataWrapper
        (Decode.field "startTime" timeDecoder)
        (Decode.field "endTime" timeDecoder)
        (Decode.field "tabs" (Decode.list tabDataDecoder))


{-| JSON decoder for TabData
-}
tabDataDecoder : Decode.Decoder TabData
tabDataDecoder =
    Decode.map2 TabData
        (Decode.field "tabName" Decode.string)
        (Decode.field "blocks" (Decode.list blockDataDecoder))


{-| JSON decoder for BlockData
-}
blockDataDecoder : Decode.Decoder BlockData
blockDataDecoder =
    Decode.map2 BlockData
        (Decode.field "blockName" Decode.string)
        (Decode.field "blockTimes" (Decode.list timeRangeDecoder))


{-| JSON decoder for TimeRange
-}
timeRangeDecoder : Decode.Decoder TimeRange
timeRangeDecoder =
    Decode.map2 TimeRange
        (Decode.field "from" timeDecoder)
        (Decode.field "to" timeDecoder)


{-| Loads fetched DataWrapper to the timeline record
-}
loadToTimeline : Timeline -> DataWrapper -> Timeline
loadToTimeline timeline data =
    let
        delimiters =
            getDelimitersFromData data.tabs data.startTime data.endTime
    in
    Timeline
        data.startTime
        data.endTime
        (TimeFormValues (timeToString data.startTime) (timeToString data.endTime))
        delimiters
        timeline.movingDelimiter
        (getTabsFromData timeline.timeStart timeline.tabs data.tabs delimiters)
        timeline.selectedTabID


getDelimitersFromData : List TabData -> Time -> Time -> List Delimiter
getDelimitersFromData tabs start end =
    let
        startDelimiter =
            Delimiters.startDelimiter start

        endDelimiter =
            Delimiters.endDelimiter end

        basicDelimiters =
            [ ( startDelimiter.id, startDelimiter )
            , ( endDelimiter.id, endDelimiter )
            ]
    in
    tabs
        |> List.map (\tab -> getDelimitersFromBlocks tab.blocks)
        |> List.foldl Dict.union (Dict.fromList basicDelimiters)
        -- remove start and end in case they were added from data
        |> Dict.remove (computeID startDelimiter.time)
        |> Dict.remove (computeID endDelimiter.time)
        |> Dict.values


getDelimitersFromBlocks : List BlockData -> Dict Int Delimiter
getDelimitersFromBlocks blocks =
    blocks
        |> List.concatMap extractBlockTimes
        |> Dict.fromList
        |> Dict.map Delimiter


extractBlockTimes : BlockData -> List ( Int, Time )
extractBlockTimes { blockTimes } =
    List.concatMap (\{ from, to } -> [ addIDToTime from, addIDToTime to ]) blockTimes


addIDToTime : Time -> ( Int, Time )
addIDToTime time =
    ( computeID time, time )


computeID : Time -> Int
computeID { hour, minute } =
    hour * 60 + minute


getTabsFromData : Time -> List Tab -> List TabData -> List Delimiter -> List Tab
getTabsFromData timeStart tabs tabData delimiters =
    tabs
        |> List.map (fillTabFromData timeStart tabData delimiters)


fillTabFromData : Time -> List TabData -> List Delimiter -> Tab -> Tab
fillTabFromData timeStart tabData delimiters tab =
    let
        thisTabData =
            getTabData tabData tab.name
    in
    { tab | blocks = getBlocks timeStart thisTabData.blocks delimiters tab.defaultBlockNames }


getTabData : List TabData -> String -> TabData
getTabData tabData name =
    tabData
        |> List.filter (\{ tabName } -> tabName == name)
        |> List.head
        |> Maybe.withDefault (TabData name [])


getBlocks : Time -> List BlockData -> List Delimiter -> List String -> List Block
getBlocks timeStart blockData delimiters defaultNames =
    let
        sortedDelimiters =
            List.sortBy (\{ time } -> timeToYPos timeStart time) delimiters

        falseStartId =
            computeID timeStart

        blocks =
            getBlocksInfo blockData defaultNames falseStartId
    in
    sortedDelimiters
        |> List.tail
        |> Maybe.map (List.map2 (createBlock blocks) sortedDelimiters)
        |> Maybe.withDefault []


getBlocksInfo : List BlockData -> List String -> Int -> List ( String, BlockID, Maybe BlockID )
getBlocksInfo blockData defaultNames falseStartId =
    blockData
        |> List.concatMap (getBlockInfo defaultNames)
        |> List.map
            (\( name, id, refId ) ->
                let
                    refIdUpdated =
                        Maybe.map (changeFalseStartId falseStartId) refId

                    idUpdated =
                        changeFalseStartId falseStartId id
                in
                ( name, idUpdated, refIdUpdated )
            )


changeFalseStartId : Int -> Int -> Int
changeFalseStartId falseId id =
    if id == falseId then
        Delimiters.startDelimiterId

    else
        id


getBlockInfo : List String -> BlockData -> List ( String, BlockID, Maybe BlockID )
getBlockInfo defaultNames { blockName, blockTimes } =
    let
        blockIds =
            List.map (\{ from } -> computeID from) blockTimes
    in
    if List.member blockName defaultNames then
        List.map (\id -> ( blockName, id, Nothing )) blockIds

    else
        List.map (\id -> ( blockName, id, List.head blockIds )) blockIds


createBlock : List ( String, BlockID, Maybe BlockID ) -> Delimiter -> Delimiter -> Block
createBlock blocks from to =
    let
        blockUse =
            blocks
                |> List.filter (\( _, id, _ ) -> id == from.id)
                |> List.head
    in
    case blockUse of
        Just ( name, id, Just refId ) ->
            if refId == id then
                Block id from to (Just <| BlockName name)

            else
                Block id from to (Just <| ReferenceBlockName name (Just refId))

        Just ( name, id, Nothing ) ->
            Block id from to (Just <| ReferenceBlockName name Nothing)

        Nothing ->
            Block from.id from to Nothing



-- ENCODING part


{-| JSON encoder for DataWrapper
-}
dataEncoder : DataWrapper -> Encode.Value
dataEncoder data =
    Encode.object
        [ ( "startTime", timeEncoder data.startTime )
        , ( "endTime", timeEncoder data.endTime )
        , ( "tabs", Encode.list tabDataEncoder data.tabs )
        ]


{-| JSON encoder for TabData
-}
tabDataEncoder : TabData -> Encode.Value
tabDataEncoder tabData =
    Encode.object
        [ ( "tabName", Encode.string tabData.tabName )
        , ( "blocks", Encode.list blockDataEncoder tabData.blocks )
        ]


{-| JSON encoder for BlockData
-}
blockDataEncoder : BlockData -> Encode.Value
blockDataEncoder blockData =
    Encode.object
        [ ( "blockName", Encode.string blockData.blockName )
        , ( "blockTimes", Encode.list timeRangeEncoder blockData.blockTimes )
        ]


{-| JSON encoder for TimeRange
-}
timeRangeEncoder : TimeRange -> Encode.Value
timeRangeEncoder timeRange =
    Encode.object
        [ ( "from", timeEncoder timeRange.from )
        , ( "to", timeEncoder timeRange.to )
        ]


{-| JSON encoder for Time
-}
timeEncoder : Time -> Encode.Value
timeEncoder time =
    Encode.object
        [ ( "hour", Encode.int time.hour )
        , ( "minute", Encode.int time.minute )
        ]


{-| Creates DataWrapper record
-}
timelineToData : Timeline -> DataWrapper
timelineToData timeline =
    DataWrapper
        timeline.timeStart
        timeline.timeEnd
        (List.map tabToTabData timeline.tabs)


tabToTabData : Tab -> TabData
tabToTabData tab =
    TabData
        tab.name
        (blocksToBlockData tab.blocks)


blocksToBlockData : List Block -> List BlockData
blocksToBlockData blocks =
    let
        namedBlocks =
            List.filterMap getNamedBlock blocks

        names =
            namedBlocks
                |> List.map Tuple.first
                |> Set.fromList
                |> Set.toList
    in
    names
        |> List.map (\name -> ( name, getAllTimesByName name namedBlocks ))
        |> List.map (\( name, times ) -> BlockData name times)


getNamedBlock : Block -> Maybe ( String, Block )
getNamedBlock block =
    block.label
        |> Maybe.map Blocks.getLabelName
        |> Maybe.map (\name -> ( name, block ))


getAllTimesByName : String -> List ( String, Block ) -> List TimeRange
getAllTimesByName name namedBlocks =
    namedBlocks
        |> List.filter (\( blockName, _ ) -> blockName == name)
        |> List.map (\( _, { from, to } ) -> TimeRange from.time to.time)
