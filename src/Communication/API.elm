module Communication.API exposing (createApi, getStructure, handleResponse, postData)

import Common exposing (API, APIResponse(..), Model, Msg(..), URL)
import Communication.Data as Data exposing (dataEncoder)
import Communication.Structure as Structure
import Http


{-| Create an API from String URL
-}
createApi : URL -> API
createApi url =
    url


{-| Create complete URL to API endpoint for structure
-}
structureFullURL : API -> URL
structureFullURL api =
    api ++ "/structure"


{-| Create complete URL to API endpoint for data
-}
dataFullURL : API -> URL
dataFullURL api =
    api ++ "/data"


{-| GET request to fetch the structure
-}
getStructure : API -> Cmd Msg
getStructure api =
    Http.get
        { url = structureFullURL api
        , expect = Http.expectJson (\structure -> GotResponse <| StructureGetResponse structure) Structure.structureDecoder
        }


{-| GET request to fetch the data
-}
getData : API -> Cmd Msg
getData api =
    Http.get
        { url = dataFullURL api
        , expect = Http.expectJson (\data -> GotResponse <| DataGetResponse data) Data.dataDecoder
        }


{-| Handling of all Msg responses created by API
-}
handleResponse : APIResponse -> Model -> ( Model, Cmd Msg )
handleResponse response model =
    case response of
        StructureGetResponse (Ok structure) ->
            ( { model | timeline = Structure.createTimeline structure }, getData model.api )

        StructureGetResponse (Err error) ->
            let
                _ =
                    Debug.log "Structure fetch Error" error
            in
            ( model, Cmd.none )

        DataGetResponse (Ok data) ->
            ( { model | timeline = Data.loadToTimeline model.timeline data }, Cmd.none )

        DataGetResponse (Err error) ->
            let
                _ =
                    Debug.log "Data fetch Error" error
            in
            ( model, Cmd.none )

        DataPostResponse (Ok ()) ->
            ( model, Cmd.none )

        DataPostResponse (Err error) ->
            let
                _ =
                    Debug.log "Data post Error" error
            in
            ( model, Cmd.none )


{-| POST request to post the data
-}
postData : Model -> Cmd Msg
postData { api, timeline } =
    Http.post
        { url = dataFullURL api
        , body = Http.jsonBody <| dataEncoder (Data.timelineToData timeline)
        , expect = Http.expectWhatever (\data -> GotResponse <| DataPostResponse data)
        }
