module Communication.Structure exposing (createTimeline, structureDecoder, timeDecoder)

import Common exposing (Block, Structure, Tab, TabID, TabSkeleton, Time, TimeFormValues, Timeline, timeToString)
import Delimiters
import Json.Decode as Decode
import List
import Tabs


{-| JSON decoder for Structure
-}
structureDecoder : Decode.Decoder Structure
structureDecoder =
    Decode.map4 Structure
        (Decode.field "mainTab" tabDecoder)
        (Decode.field "defaultStartTime" timeDecoder)
        (Decode.field "defaultEndTime" timeDecoder)
        (Decode.field "tabs" (Decode.list tabDecoder))


{-| JSON decoder for TabSkeleton
-}
tabDecoder : Decode.Decoder TabSkeleton
tabDecoder =
    Decode.map3 TabSkeleton
        (Decode.field "tabName" Decode.string)
        (Decode.field "canAdd" Decode.bool)
        (Decode.field "defaultNames" (Decode.list Decode.string))


{-| JSON decoder for Time
-}
timeDecoder : Decode.Decoder Time
timeDecoder =
    Decode.map2 Time
        (Decode.field "hour" Decode.int)
        (Decode.field "minute" Decode.int)


{-| Creates a new Timeline from a fetched Structure
-}
createTimeline : Structure -> Timeline
createTimeline structure =
    let
        startTime =
            structure.defaultStartTime

        endTime =
            structure.defaultEndTime

        startDelimiter =
            Delimiters.startDelimiter startTime

        endDelimiter =
            Delimiters.endDelimiter endTime

        mainTab =
            Tab
                Tabs.mainTabID
                structure.mainTab.tabName
                structure.mainTab.canAdd
                [ Block 0 startDelimiter endDelimiter Nothing ]
                structure.mainTab.defaultNames
    in
    Timeline
        startTime
        endTime
        (TimeFormValues (timeToString startTime) (timeToString endTime))
        [ startDelimiter, endDelimiter ]
        Nothing
        (mainTab :: createTabs [ Block 0 startDelimiter endDelimiter Nothing ] structure.tabs (Tabs.mainTabID + 1))
        Tabs.mainTabID


createTabs : List Block -> List TabSkeleton -> TabID -> List Tab
createTabs blocks tabSkeletons nextId =
    List.range nextId (nextId + List.length tabSkeletons)
        |> List.map2 (createTab blocks) tabSkeletons


createTab : List Block -> TabSkeleton -> TabID -> Tab
createTab blocks tabSkeleton tabID =
    Tab
        tabID
        tabSkeleton.tabName
        tabSkeleton.canAdd
        blocks
        tabSkeleton.defaultNames
