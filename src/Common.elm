module Common exposing (..)

{-| This module contains most types and simple utility functions
that are commonly used in multiple other modules, preventing cyclic dependency
-}

import Http
import String


{-| Default height of input field for block desctription

Also determines the minimal size of a block (in px, not in minutes)

-}
inputHeight : Float
inputHeight =
    30


{-| Default height for use in styles files
-}
inputHeightDistance : String
inputHeightDistance =
    String.fromFloat inputHeight ++ "px"


{-| Application Messages
-}
type Msg
    = StartTimeChange
    | EndTimeChange
    | TimelineDown Coordinates
    | TimelineMove Coordinates
    | TimelineUp
    | TimeInput TimeFormInput String
    | SelectReferenceBlockName BlockID String
    | AddNewBlockName BlockID
    | BlockInput BlockID String
    | SelectTab TabID
    | GotResponse APIResponse


{-| API response submessages
-}
type APIResponse
    = StructureGetResponse (Result Http.Error Structure)
    | DataGetResponse (Result Http.Error DataWrapper)
    | DataPostResponse (Result Http.Error ())


{-| Submessages for input into the start and end time forms
-}
type TimeFormInput
    = StartTimeForm
    | EndTimeForm


{-| Used for cursor tracking
-}
type alias Coordinates =
    { x : Float
    , y : Float
    }


{-| Representation of Time value
-}
type alias Time =
    { hour : Int
    , minute : Int
    }


{-| Representation of Delimiter, used for blocks
-}
type alias Delimiter =
    { id : Int
    , time : Time
    }


{-| Representation of a moving Delimiter, while it is being edited

Can move only in between the from and to delimiters

-}
type alias MovingDelimiter =
    { delimiter : Delimiter
    , from : Delimiter
    , to : Delimiter
    }


{-| Representation of a block of time, restricted by delimiters
-}
type alias Block =
    { id : BlockID
    , from : Delimiter
    , to : Delimiter
    , label : Maybe BlockLabel
    }


{-| Representation of Label for blocks, either input or a reference to
existing (or default if the reference id is nothing) label
-}
type BlockLabel
    = ReferenceBlockName String (Maybe BlockID)
    | BlockName String


{-| Representation of an ID of a block
-}
type alias BlockID =
    Int


{-| Representation of Tab

canAdd determines if new block names can be added to the blocks

-}
type alias Tab =
    { id : TabID
    , name : String
    , canAdd : Bool
    , blocks : List Block
    , defaultBlockNames : List String
    }


{-| Representation of an ID of a Tab
-}
type alias TabID =
    Int


{-| Representation of the Timeline

movingDelimiter is used for added and edited delimiters

-}
type alias Timeline =
    { timeStart : Time
    , timeEnd : Time
    , formValues : TimeFormValues
    , delimiters : List Delimiter
    , movingDelimiter : Maybe MovingDelimiter
    , tabs : List Tab
    , selectedTabID : TabID
    }


{-| The widget model
-}
type alias Model =
    { timeline : Timeline
    , api : API
    }


{-| Representation of an API
-}
type alias API =
    URL


{-| Representation of an URL
-}
type alias URL =
    String


{-| Representation of input values in the time inputs
-}
type alias TimeFormValues =
    { startTimeValue : String
    , endTimeValue : String
    }


{-| Representation of a Structure

Used to retrieve JSON data from the server, represents the timeline structure

-}
type alias Structure =
    { mainTab : TabSkeleton
    , defaultStartTime : Time
    , defaultEndTime : Time
    , tabs : List TabSkeleton
    }


{-| Representation of a Tab, but only with the data retrievable from the server
-}
type alias TabSkeleton =
    { tabName : String
    , canAdd : Bool
    , defaultNames : List String
    }


{-| Representation of timeline Data
-}
type alias DataWrapper =
    { startTime : Time
    , endTime : Time
    , tabs : List TabData
    }


{-| Representation of a Data in a Tab
-}
type alias TabData =
    { tabName : String
    , blocks : List BlockData
    }


{-| Representation of a Data in a Block
-}
type alias BlockData =
    { blockName : String
    , blockTimes : List TimeRange
    }


{-| Representation of a Time Range
-}
type alias TimeRange =
    { from : Time
    , to : Time
    }


{-| True if the time is within the time range

If the range start is after end, end is increased by 24 hours

-}
inTimeRange : ( Time, Time ) -> Time -> Bool
inTimeRange ( from, to ) time =
    let
        toExtended =
            if timeBefore to from then
                { to | hour = to.hour + 24 }

            else
                to
    in
    timeBefore from time && timeBefore time toExtended


{-| Parse string in HH:MM format into Time
-}
timeFromString : String -> Maybe Time
timeFromString str =
    str
        |> String.split ":"
        |> List.map String.toInt
        |> timeFromList


{-| Helper function to the string parser, converts list of maybe ints into time
-}
timeFromList : List (Maybe Int) -> Maybe Time
timeFromList hourMinute =
    let
        hour =
            hourMinute
                |> List.head
                |> Maybe.withDefault Nothing

        minute =
            hourMinute
                |> List.tail
                |> Maybe.andThen List.head
                |> Maybe.withDefault Nothing
    in
    Maybe.map2 (\a b -> Time a b) hour minute


{-| Create String represenatation in HH:MM format from Time
-}
timeToString : Time -> String
timeToString time =
    let
        hour =
            modBy 24 time.hour

        strHour =
            if hour >= 10 then
                String.fromInt hour

            else
                "0" ++ String.fromInt hour

        strMinute =
            if time.minute >= 10 then
                String.fromInt time.minute

            else
                "0" ++ String.fromInt time.minute
    in
    strHour ++ ":" ++ strMinute


{-| Check if timeL < timeR
-}
timeBefore : Time -> Time -> Bool
timeBefore timeL timeR =
    timeL.hour < timeR.hour || (timeL.hour == timeR.hour && timeL.minute < timeR.minute)


{-| Compute the difference between times in minutes
-}
computeMinuteDiff : Time -> Time -> Int
computeMinuteDiff start end =
    let
        endExtended =
            if timeBefore end start then
                { end | hour = end.hour + 24 }

            else
                end
    in
    (endExtended.hour - start.hour) * 60 + (endExtended.minute - start.minute)


{-| convert minute difference into string with pixels, used in styling
-}
minuteDiffToDistance : Int -> String
minuteDiffToDistance minuteDiff =
    String.fromFloat (minuteDiffToYPos minuteDiff) ++ "px"


{-| Determine the respective time from a Y position in the widget
-}
yPosToTime : Time -> Float -> Time
yPosToTime timeStart yPos =
    minuteDiffToTime timeStart <| yPosToMinuteDiff yPos


{-| Compute the Y position in the widget of a certain time
-}
timeToYPos : Time -> Time -> Float
timeToYPos timeStart time =
    minuteDiffToYPos <| computeMinuteDiff timeStart time


{-| Check if 2 positions are not too close to each other, based on inputHeihgt
-}
tooCloseYPos : Float -> Float -> Bool
tooCloseYPos pos1 pos2 =
    yPosDistance pos1 pos2 < inputHeight


{-| Check if position is inbetween 2 positions with inputHeight space on each side
-}
inMarginRange : Float -> Float -> Float -> Bool
inMarginRange from to yPos =
    (yPos - from > inputHeight)
        && (to - yPos > inputHeight)


{-| Compute a distance between 2 Y positions
-}
yPosDistance : Float -> Float -> Float
yPosDistance pos1 pos2 =
    abs (pos1 - pos2)


{-| Convert Y position to minute difference
-}
yPosToMinuteDiff : Float -> Int
yPosToMinuteDiff yPos =
    floor (yPos / 2)


{-| Convert minute difference to Y position
-}
minuteDiffToYPos : Int -> Float
minuteDiffToYPos minuteDiff =
    toFloat (minuteDiff * 2)


{-| Convert minute difference to Time
-}
minuteDiffToTime : Time -> Int -> Time
minuteDiffToTime timeStart minuteDiff =
    let
        startMinuntes =
            60 - timeStart.minute

        restMinutes =
            minuteDiff - startMinuntes

        hours =
            if restMinutes >= 0 then
                restMinutes // 60 + 1

            else
                0

        finalMinutes =
            modBy 60 restMinutes
    in
    Time (timeStart.hour + hours) finalMinutes


{-| Compute the offset of a block input in the middle
of the space reserved to the block, used for styling
-}
computeBlockOffset : Time -> Time -> Time -> Int
computeBlockOffset timeStart blockStart blockEnd =
    let
        startMinuteDiff =
            computeMinuteDiff timeStart blockStart

        widthMinuteDiff =
            computeMinuteDiff blockStart blockEnd
    in
    startMinuteDiff + (widthMinuteDiff - yPosToMinuteDiff inputHeight) // 2
