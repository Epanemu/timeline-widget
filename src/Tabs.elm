module Tabs exposing (changeEndBlocks, changeStartBlocks, mainTabID, setLabel, setReferenceLabel, splitBlocks, viewBlocks)

import Blocks
import Common exposing (BlockID, MovingDelimiter, Msg(..), Tab, TabID, Time)
import Dict exposing (Dict)
import Html exposing (Html)


{-| TabID of the main tab, required to determine the main tab reliably
-}
mainTabID : TabID
mainTabID =
    -1


{-| Ammends the start block in all tabs
-}
changeStartBlocks : List Tab -> Time -> Time -> List Tab
changeStartBlocks tabs oldTime newTime =
    List.map (\tab -> { tab | blocks = Blocks.changeStartBlock tab.blocks oldTime newTime }) tabs


{-| Ammends the start block in all tabs
-}
changeEndBlocks : List Tab -> Time -> Time -> List Tab
changeEndBlocks tabs oldTime newTime =
    List.map (\tab -> { tab | blocks = Blocks.changeEndBlock tab.blocks oldTime newTime }) tabs


{-| Splits the same block in all tabs
-}
splitBlocks : List Tab -> MovingDelimiter -> List Tab
splitBlocks tabs movingDelimiter =
    List.map (\tab -> { tab | blocks = Blocks.splitBlock tab.blocks movingDelimiter }) tabs


{-| Intermediary function that finds the active tab and sets reference label only in that tab
-}
setReferenceLabel : List Tab -> TabID -> BlockID -> String -> Maybe BlockID -> List Tab
setReferenceLabel tabs tabID blockID name targetId =
    List.map
        (\tab ->
            if tab.id == tabID then
                { tab | blocks = Blocks.setReferenceLabel tab.blocks blockID name targetId }

            else
                tab
        )
        tabs


{-| Intermediary function that finds the active tab and sets label only in that appropriate tab
-}
setLabel : List Tab -> TabID -> BlockID -> String -> List Tab
setLabel tabs tabID blockID name =
    List.map
        (\tab ->
            if tab.id == tabID then
                { tab | blocks = Blocks.setLabel tab.blocks blockID name }

            else
                tab
        )
        tabs


{-| View fucntion for viewing a block
-}
viewBlocks : Time -> List Tab -> TabID -> List (Html Msg)
viewBlocks timeStart tabs tabID =
    let
        mainInfo =
            if tabID == mainTabID then
                Nothing

            else
                Just <| collectMainTabNames tabs
    in
    tabs
        |> List.filter (\tab -> tab.id == tabID)
        |> List.head
        |> Maybe.map (\tab -> Blocks.viewBlocks timeStart tab.blocks mainInfo tab.canAdd tab.defaultBlockNames)
        |> Maybe.withDefault [ Html.text "" ]


collectMainTabNames : List Tab -> Dict BlockID String
collectMainTabNames tabs =
    tabs
        |> List.filter (\tab -> tab.id == mainTabID)
        |> List.map .blocks
        |> List.head
        |> Maybe.withDefault []
        |> List.filterMap
            (\block ->
                Maybe.map
                    (\label -> ( block.id, Blocks.getLabelName label ))
                    block.label
            )
        |> Dict.fromList
