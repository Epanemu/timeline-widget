module Styles exposing
    ( blockButton
    , blockInfo
    , blockNameWrapper
    , blockOptions
    , blockSelect
    , blocksWrapper
    , content
    , delimiter
    , delimiterLabel
    , hourTick
    , mainTimeBar
    , mainTimeBarWrapper
    , tabBarElement
    , tabBarSelectedElement
    , tabBarWrapper
    , timeInput
    , timelineWrapper
    )

import Common exposing (inputHeightDistance, minuteDiffToDistance)
import Html exposing (Attribute)
import Html.Attributes as Attributes


{-| Styles of the content wrapper
-}
content : List (Attribute msg)
content =
    [ Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "column"
    , Attributes.style "min-width" "500px"
    ]


{-| Styles of the timeline wrapper
-}
timelineWrapper : List (Attribute msg)
timelineWrapper =
    [ Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "column"
    , Attributes.style "margin-left" "4em"
    , Attributes.style "margin-right" "400px"
    , Attributes.style "align-items" "center"
    ]


{-| Styles of the timeline and blocks wrapper
-}
mainTimeBarWrapper : List (Attribute msg)
mainTimeBarWrapper =
    [ Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "row"
    ]


{-| Styles of the time start and end input
-}
timeInput : List (Attribute msg)
timeInput =
    [ Attributes.style "width" "3em"
    , Attributes.style "text-align" "center"
    , Attributes.style "margin" "0.5em"
    ]


{-| Styles of the timeline block
-}
mainTimeBar : Int -> List (Attribute msg)
mainTimeBar minuteDiff =
    [ Attributes.style "width" "2em"
    , Attributes.style "border" "black solid 2px"
    , Attributes.style "position" "relative"
    , Attributes.style "height" (minuteDiffToDistance minuteDiff)
    ]


{-| Styles of an hour tick
-}
hourTick : Int -> List (Attribute msg)
hourTick minuteDiff =
    [ Attributes.style "width" "100%"
    , Attributes.style "padding" "2px"
    , Attributes.style "box-sizing" "border-box"
    , Attributes.style "border-top" "gray solid 2px"
    , Attributes.style "color" "gray"
    , Attributes.style "text-align" "center"
    , Attributes.style "position" "absolute"
    , Attributes.style "top" (minuteDiffToDistance minuteDiff)
    , Attributes.style "z-index" "-1"
    ]


{-| Styles of a delimiter
-}
delimiter : Int -> List (Attribute msg)
delimiter minuteDiff =
    [ Attributes.style "width" "100%"
    , Attributes.style "box-sizing" "border-box"
    , Attributes.style "border-top" "black solid 2px"
    , Attributes.style "position" "absolute"
    , Attributes.style "top" (minuteDiffToDistance minuteDiff)
    , Attributes.style "z-index" "-1"
    ]


{-| Styles of a delimiter label
-}
delimiterLabel : List (Attribute msg)
delimiterLabel =
    [ Attributes.style "position" "relative"
    , Attributes.style "right" "3.5em"
    , Attributes.style "z-index" "-1"
    , Attributes.style "font-size" ".8em"
    , Attributes.style "line-height" ".8em"
    , Attributes.style "top" "-.4em"
    , Attributes.style "margin-right" "2px"
    ]


{-| Styles of a blocks wrapper
-}
blocksWrapper : List (Attribute msg)
blocksWrapper =
    [ Attributes.style "height" "100%"
    , Attributes.style "position" "relative"
    ]


{-| Styles of a block wrapper
-}
blockNameWrapper : Int -> List (Attribute msg)
blockNameWrapper offsetMinuteDiff =
    [ Attributes.style "height" inputHeightDistance
    , Attributes.style "width" "400px"
    , Attributes.style "margin-left" "1em"
    , Attributes.style "position" "absolute"
    , Attributes.style "top" (minuteDiffToDistance offsetMinuteDiff)
    , Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "row"
    ]


{-| Styles of an unassigned block name
-}
blockOptions : List (Attribute msg)
blockOptions =
    [ Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "row"
    ]


{-| Styles of a block "Add New" button
-}
blockButton : List (Attribute msg)
blockButton =
    [ Attributes.style "margin-right" "5px"
    , Attributes.style "width" "95px"
    ]


{-| Styles of a block reference selector
-}
blockSelect : List (Attribute msg)
blockSelect =
    [ Attributes.style "width" "200px"
    ]


{-| Styles of a block information (input from the main tab)
-}
blockInfo : List (Attribute msg)
blockInfo =
    [ Attributes.style "font-size" "x-small"
    , Attributes.style "padding" "0 3px"
    , Attributes.style "width" "100px"
    ]


{-| Styles of a tab bar wrapping div
-}
tabBarWrapper : List (Attribute msg)
tabBarWrapper =
    [ Attributes.style "display" "flex"
    , Attributes.style "flex-direction" "row"
    , Attributes.style "justify-content" "center"
    , Attributes.style "margin-bottom" "5px"
    ]


{-| Common styles of a tab heading element
-}
tabBarElementCommon : List (Attribute msg)
tabBarElementCommon =
    [ Attributes.style "border" "grey solid 1px"
    , Attributes.style "padding" "8px"
    , Attributes.style "margin" "0 5px"
    ]


{-| Styles of a tab bar unselected element
-}
tabBarElement : List (Attribute msg)
tabBarElement =
    tabBarElementCommon
        ++ [ Attributes.style "color" "blue"
           ]


{-| Styles of a tab bar selected element
-}
tabBarSelectedElement : List (Attribute msg)
tabBarSelectedElement =
    tabBarElementCommon
        ++ [ Attributes.style "color" "black"
           , Attributes.style "font-weight" "bold"
           ]
