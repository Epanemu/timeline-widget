module Main exposing (main)

import Blocks
import Browser
import Common exposing (Block, Model, Msg(..), Tab, TabID, Time, TimeFormInput(..), TimeFormValues, Timeline, computeMinuteDiff, timeFromString, timeToString)
import Communication.API as API
import Delimiters
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Html.Events.Extra.Pointer as Pointer
import PointerExtra
import Styles
import Tabs



---- MODEL ----


type alias InitialValues =
    { baseUrl : String
    }


init : InitialValues -> ( Model, Cmd Msg )
init { baseUrl } =
    let
        defaultStartTime =
            Time 8 0

        defaultEndTime =
            Time 0 0

        defaultStartDelimiter =
            Delimiters.startDelimiter defaultStartTime

        defaultEndDelimiter =
            Delimiters.endDelimiter defaultEndTime

        defaultMainTab =
            Tab
                Tabs.mainTabID
                "MainTab"
                True
                [ Block 0 defaultStartDelimiter defaultEndDelimiter Nothing ]
                []

        api =
            API.createApi baseUrl
    in
    ( { timeline =
            Timeline
                defaultStartTime
                defaultEndTime
                (TimeFormValues (timeToString defaultStartTime)
                    (timeToString defaultEndTime)
                )
                [ defaultStartDelimiter, defaultEndDelimiter ]
                Nothing
                [ defaultMainTab ]
                Tabs.mainTabID
      , api = api
      }
    , API.getStructure api
    )



---- UPDATE ----


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ timeline } as model) =
    case msg of
        StartTimeChange ->
            let
                newTime =
                    timeline.formValues.startTimeValue
                        |> timeFromString
                        |> Maybe.withDefault timeline.timeStart

                oldForms =
                    timeline.formValues
            in
            ( { model
                | timeline =
                    { timeline
                        | timeStart = newTime
                        , formValues = { oldForms | startTimeValue = timeToString newTime }
                        , delimiters = Delimiters.changeStartDelimiter timeline.delimiters newTime
                        , tabs = Tabs.changeStartBlocks timeline.tabs timeline.timeStart newTime
                    }
              }
            , Cmd.none
            )

        EndTimeChange ->
            let
                newTime =
                    timeline.formValues.endTimeValue
                        |> timeFromString
                        |> Maybe.withDefault timeline.timeEnd

                oldForms =
                    timeline.formValues
            in
            ( { model
                | timeline =
                    { timeline
                        | timeEnd = newTime
                        , formValues = { oldForms | endTimeValue = timeToString newTime }
                        , delimiters = Delimiters.changeEndDelimiter timeline.delimiters newTime
                        , tabs = Tabs.changeEndBlocks timeline.tabs timeline.timeEnd newTime
                    }
              }
            , Cmd.none
            )

        TimelineDown { y } ->
            let
                movingDelimiter =
                    Delimiters.newOrCloseExisting timeline.timeStart timeline.timeEnd timeline.delimiters y

                delimiters =
                    List.filter (\delimiter -> delimiter.id /= movingDelimiter.delimiter.id) timeline.delimiters
            in
            if Delimiters.isBorderDelimiter movingDelimiter.delimiter then
                ( model, Cmd.none )

            else
                ( { model
                    | timeline =
                        { timeline
                            | movingDelimiter = Just <| Delimiters.moveDelimiter timeline.timeStart y movingDelimiter
                            , delimiters = delimiters
                        }
                  }
                , Cmd.none
                )

        TimelineMove { y } ->
            ( { model
                | timeline =
                    { timeline
                        | movingDelimiter =
                            Maybe.map
                                (Delimiters.moveDelimiter timeline.timeStart y)
                                timeline.movingDelimiter
                    }
              }
            , Cmd.none
            )

        TimelineUp ->
            ( { model
                | timeline =
                    { timeline
                        | delimiters =
                            timeline.movingDelimiter
                                |> Maybe.map (\movingD -> Delimiters.setDelimiter timeline.delimiters movingD.delimiter)
                                |> Maybe.withDefault timeline.delimiters
                        , movingDelimiter = Nothing
                        , tabs =
                            timeline.movingDelimiter
                                |> Maybe.map (\movingD -> Tabs.splitBlocks timeline.tabs movingD)
                                |> Maybe.withDefault timeline.tabs
                    }
              }
            , Cmd.none
            )

        TimeInput input value ->
            let
                oldForms =
                    timeline.formValues
            in
            ( { model | timeline = { timeline | formValues = updateFormValues oldForms input value } }, Cmd.none )

        SelectReferenceBlockName blockID value ->
            let
                ( name, targetId ) =
                    Blocks.parseValue value
            in
            ( { model | timeline = { timeline | tabs = Tabs.setReferenceLabel timeline.tabs timeline.selectedTabID blockID name targetId } }, Cmd.none )

        AddNewBlockName blockID ->
            ( { model | timeline = { timeline | tabs = Tabs.setLabel timeline.tabs timeline.selectedTabID blockID "" } }, Cmd.none )

        BlockInput blockID value ->
            ( { model | timeline = { timeline | tabs = Tabs.setLabel timeline.tabs timeline.selectedTabID blockID value } }, Cmd.none )

        SelectTab tabID ->
            ( { model | timeline = { timeline | selectedTabID = tabID } }, API.postData model )

        GotResponse response ->
            API.handleResponse response model


updateFormValues : TimeFormValues -> TimeFormInput -> String -> TimeFormValues
updateFormValues oldValues input value =
    case input of
        StartTimeForm ->
            { oldValues | startTimeValue = value }

        EndTimeForm ->
            { oldValues | endTimeValue = value }



---- VIEW ----


view : Model -> Html Msg
view { timeline } =
    Html.div
        (Styles.content ++ [ Pointer.onUp (\_ -> TimelineUp) ])
        [ viewTabsBar timeline.tabs timeline.selectedTabID
        , viewMainLine timeline
        ]


viewTabsBar : List Tab -> TabID -> Html Msg
viewTabsBar tabs selectedTabID =
    Html.div Styles.tabBarWrapper <|
        List.map (viewTab selectedTabID) tabs


viewTab : TabID -> Tab -> Html Msg
viewTab id tab =
    if id == tab.id then
        Html.a Styles.tabBarSelectedElement [ Html.text tab.name ]

    else
        Html.a (Styles.tabBarElement ++ [ Events.onClick (SelectTab tab.id) ]) [ Html.text tab.name ]


viewMainLine : Timeline -> Html Msg
viewMainLine { formValues, timeStart, timeEnd, delimiters, movingDelimiter, tabs, selectedTabID } =
    let
        minuteDiff =
            computeMinuteDiff timeStart timeEnd
    in
    Html.div Styles.timelineWrapper
        [ Html.input
            (Styles.timeInput
                ++ [ Attributes.value formValues.startTimeValue
                   , Events.onInput (TimeInput StartTimeForm)
                   , Events.onBlur StartTimeChange
                   ]
            )
            []
        , Html.div Styles.mainTimeBarWrapper
            [ Html.div
                (Styles.mainTimeBar minuteDiff
                    ++ [ Pointer.onDown (PointerExtra.relativePos >> TimelineDown)
                       , Pointer.onMove (PointerExtra.relativePos >> TimelineMove)
                       , Attributes.style "touch-action" "none"
                       ]
                )
                (viewHourTicks timeStart minuteDiff
                    ++ Delimiters.viewDelimiters timeStart timeEnd delimiters
                    ++ [ Delimiters.viewMovingDelimiter timeStart movingDelimiter ]
                )
            , Html.div Styles.blocksWrapper
                (Tabs.viewBlocks timeStart tabs selectedTabID)
            ]
        , Html.input
            (Styles.timeInput
                ++ [ Attributes.value formValues.endTimeValue
                   , Events.onInput (TimeInput EndTimeForm)
                   , Events.onBlur EndTimeChange
                   ]
            )
            []
        ]


viewHourTicks : Time -> Int -> List (Html Msg)
viewHourTicks start minuteDiff =
    let
        startDiff =
            60 - start.minute
    in
    ((minuteDiff + start.minute) // 60 - 1)
        |> List.range 0
        |> List.map (\hourDiff -> ( startDiff + hourDiff * 60, start.hour + hourDiff + 1 ))
        |> List.map (\( diff, hour ) -> viewHourTick minuteDiff diff hour)


viewHourTick : Int -> Int -> Int -> Html Msg
viewHourTick totalMinuteDiff minuteDiff hour =
    Html.div (Styles.hourTick minuteDiff) [ viewTickLabel (totalMinuteDiff - minuteDiff) hour ]


viewTickLabel : Int -> Int -> Html Msg
viewTickLabel minuteDiffSpace hour =
    if minuteDiffSpace >= 10 then
        Html.text (String.fromInt (modBy 24 hour))

    else
        Html.text ""



---- PROGRAM ----


main : Program InitialValues Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = always Sub.none
        }
