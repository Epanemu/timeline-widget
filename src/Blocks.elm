module Blocks exposing (changeEndBlock, changeStartBlock, getLabelName, parseValue, setLabel, setReferenceLabel, splitBlock, viewBlocks)

import Common exposing (Block, BlockID, BlockLabel(..), Delimiter, MovingDelimiter, Msg(..), Time, computeBlockOffset)
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Styles


getBlockNameId : List Block -> List ( String, Maybe BlockID )
getBlockNameId blocks =
    List.filterMap
        (\block ->
            case block.label of
                Just (BlockName name) ->
                    Just ( name, Just block.id )

                _ ->
                    Nothing
        )
        blocks


{-| View function for blocks
-}
viewBlocks : Time -> List Block -> Maybe (Dict BlockID String) -> Bool -> List String -> List (Html Msg)
viewBlocks timeStart blocks mainInfo canAdd defaultBlockNames =
    let
        blockNames =
            List.map (\name -> ( name, Nothing )) defaultBlockNames
                ++ getBlockNameId blocks
    in
    List.map
        (\block ->
            viewBlock timeStart
                blockNames
                (Maybe.andThen (Dict.get block.id) mainInfo)
                canAdd
                block
        )
        blocks


viewBlock : Time -> List ( String, Maybe BlockID ) -> Maybe String -> Bool -> Block -> Html Msg
viewBlock timeStart blockNames infoStr canAdd block =
    let
        blockOffset =
            computeBlockOffset timeStart block.from.time block.to.time
    in
    Html.div (Styles.blockNameWrapper blockOffset)
        [ block.label
            |> Maybe.map (viewLabel block.id)
            |> Maybe.withDefault (viewNoLabel canAdd blockNames block.id)
        , infoStr
            |> Maybe.map viewInfo
            |> Maybe.withDefault viewNoInfo
        ]


viewLabel : BlockID -> BlockLabel -> Html Msg
viewLabel id label =
    case label of
        BlockName name ->
            Html.input
                [ Attributes.type_ "text"
                , Attributes.value name
                , Events.onInput (BlockInput id)
                ]
                []

        ReferenceBlockName name _ ->
            Html.input
                [ Attributes.type_ "text"
                , Attributes.disabled True
                , Attributes.value name
                ]
                []


viewNoLabel : Bool -> List ( String, Maybe BlockID ) -> BlockID -> Html Msg
viewNoLabel canAdd blockNames id =
    let
        selectDropdown =
            [ Html.select
                (Styles.blockSelect
                    ++ [ Events.onInput (SelectReferenceBlockName id)
                       ]
                )
                (selectNameOption :: namesToHtml blockNames)
            ]
    in
    Html.div
        Styles.blockOptions
        (if canAdd then
            Html.button
                (Styles.blockButton
                    ++ [ Events.onClick (AddNewBlockName id) ]
                )
                [ Html.text "Add new" ]
                :: selectDropdown

         else
            selectDropdown
        )


viewInfo : String -> Html Msg
viewInfo infoStr =
    Html.div Styles.blockInfo [ Html.text infoStr ]


viewNoInfo : Html Msg
viewNoInfo =
    Html.text ""


selectNameOption : Html Msg
selectNameOption =
    Html.option
        [ Attributes.disabled True
        , Attributes.selected True
        , Attributes.hidden True
        ]
        [ Html.text "Select existing" ]


namesToHtml : List ( String, Maybe BlockID ) -> List (Html Msg)
namesToHtml names =
    List.map
        (\( name, id ) ->
            let
                strId =
                    id
                        |> Maybe.map String.fromInt
                        |> Maybe.withDefault "NO_ID"
            in
            Html.option
                [ Attributes.value (strId ++ " " ++ name) ]
                [ Html.text name ]
        )
        names


{-| Parse value of an option from a refernece dropdown of an unassigned block
-}
parseValue : String -> ( String, Maybe BlockID )
parseValue value =
    let
        id =
            value
                |> String.split " "
                |> List.head
                |> Maybe.andThen String.toInt

        name =
            value
                |> String.split " "
                |> List.tail
                |> Maybe.map (List.intersperse " ")
                |> Maybe.map String.concat
                |> Maybe.withDefault ""
    in
    ( name, id )


{-| Retrieve a string name from either of the BlockLabel options
-}
getLabelName : BlockLabel -> String
getLabelName label =
    case label of
        BlockName name ->
            name

        ReferenceBlockName name _ ->
            name


{-| Find block that uses start time and change the from time
-}
changeStartBlock : List Block -> Time -> Time -> List Block
changeStartBlock blocks prevTime startTime =
    List.map
        (\block ->
            if block.from.time == prevTime then
                { block | from = Delimiter block.from.id startTime }

            else
                block
        )
        blocks


{-| Find block that uses end time and change the to time
-}
changeEndBlock : List Block -> Time -> Time -> List Block
changeEndBlock blocks prevTime endTime =
    List.map
        (\block ->
            if block.to.time == prevTime then
                { block | to = Delimiter block.to.id endTime }

            else
                block
        )
        blocks


{-| Split a block by adding a delimiter, or modify blocks by moving a delimiter
-}
splitBlock : List Block -> MovingDelimiter -> List Block
splitBlock blocks { delimiter, from, to } =
    let
        maybeBlock =
            blocks
                |> List.filter (\block -> block.from == from && block.to == to)
                |> List.head

        newId =
            blocks
                |> List.map .id
                |> List.maximum
                |> Maybe.withDefault 0
    in
    case maybeBlock of
        Just { id, label } ->
            List.filter (\block -> block.from /= from || block.to /= to) blocks
                ++ [ Block id from delimiter label
                   , Block (newId + 1) delimiter to <| inputToReferneceLabel label id
                   ]

        Nothing ->
            List.map
                (\block ->
                    if block.from == from then
                        { block | to = delimiter }

                    else if block.to == to then
                        { block | from = delimiter }

                    else
                        block
                )
                blocks


inputToReferneceLabel : Maybe BlockLabel -> BlockID -> Maybe BlockLabel
inputToReferneceLabel label targetId =
    case label of
        Just (BlockName name) ->
            Just <| ReferenceBlockName name (Just targetId)

        _ ->
            label


{-| Find a block by it's id and set it's label
-}
setReferenceLabel : List Block -> BlockID -> String -> Maybe BlockID -> List Block
setReferenceLabel blocks id name targetId =
    blocks
        |> List.map
            (\block ->
                if block.id == id then
                    { block | label = Just <| ReferenceBlockName name targetId }

                else
                    block
            )


{-| Find a block by it's id and set it's label
-}
setLabel : List Block -> BlockID -> String -> List Block
setLabel blocks id name =
    blocks
        |> List.map
            (\block ->
                if block.id == id then
                    { block | label = Just <| BlockName name }

                else
                    setReferenceName block id name
            )


setReferenceName : Block -> BlockID -> String -> Block
setReferenceName block referenceID name =
    { block
        | label =
            case block.label of
                Just (ReferenceBlockName _ (Just id)) ->
                    if id == referenceID then
                        Just <| ReferenceBlockName name (Just id)

                    else
                        block.label

                _ ->
                    block.label
    }
