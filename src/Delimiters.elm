module Delimiters exposing (changeEndDelimiter, changeStartDelimiter, endDelimiter, isBorderDelimiter, moveDelimiter, newOrCloseExisting, setDelimiter, startDelimiter, startDelimiterId, viewDelimiters, viewMovingDelimiter)

import Common exposing (Delimiter, MovingDelimiter, Msg, Time, computeMinuteDiff, inMarginRange, inTimeRange, minuteDiffToTime, timeBefore, timeToString, timeToYPos, tooCloseYPos, yPosToTime)
import Html exposing (Html)
import Styles


{-| Id of the start delimiter, special because this delimiter can not be moved
-}
startDelimiterId : Int
startDelimiterId =
    -2


{-| Creates a start delimiter
-}
startDelimiter : Time -> Delimiter
startDelimiter startTime =
    Delimiter startDelimiterId startTime


{-| Changes the time of the start delimiter
-}
changeStartDelimiter : List Delimiter -> Time -> List Delimiter
changeStartDelimiter delimiters startTime =
    List.map
        (\delimiter ->
            if delimiter.id == startDelimiterId then
                startDelimiter startTime

            else
                delimiter
        )
        delimiters


{-| Id of the end delimiter, special because this delimiter can not be moved
-}
endDelimiterId : Int
endDelimiterId =
    -1


{-| Creates an end delimiter
-}
endDelimiter : Time -> Delimiter
endDelimiter endTime =
    Delimiter endDelimiterId endTime


{-| Changes the time of the end delimiter
-}
changeEndDelimiter : List Delimiter -> Time -> List Delimiter
changeEndDelimiter delimiters endTime =
    List.map
        (\delimiter ->
            if delimiter.id == endDelimiterId then
                endDelimiter endTime

            else
                delimiter
        )
        delimiters


{-| Check if delimiter is start or end delimiter
-}
isBorderDelimiter : Delimiter -> Bool
isBorderDelimiter { id } =
    id < 0


{-| Create a new delimiter
-}
createNew : Time -> List Delimiter -> Float -> Delimiter
createNew startTime delimiters yPos =
    delimiters
        |> List.map .id
        |> List.maximum
        |> Maybe.withDefault 0
        |> (+) 1
        |> (\id -> Delimiter id (yPosToTime startTime yPos))


{-| Create new moving delimiter with the given time,
or if the time is close to another delimiter make that delimiter moving
-}
newOrCloseExisting : Time -> Time -> List Delimiter -> Float -> MovingDelimiter
newOrCloseExisting startTime endTime delimiters yPos =
    let
        delimiter =
            delimiters
                |> List.filter (\delim -> tooCloseYPos (timeToYPos startTime delim.time) yPos)
                |> List.head
                |> Maybe.withDefault (createNew startTime delimiters yPos)

        from =
            previousDelimiter startTime delimiter delimiters

        to =
            nextDelimiter endTime delimiter delimiters
    in
    MovingDelimiter delimiter from to


previousDelimiter : Time -> Delimiter -> List Delimiter -> Delimiter
previousDelimiter startTime delimiter delimiters =
    delimiters
        |> List.filter (\delim -> timeBefore delim.time delimiter.time)
        |> List.sortBy (\delim -> computeMinuteDiff delim.time delimiter.time)
        |> List.head
        |> Maybe.withDefault (startDelimiter startTime)


nextDelimiter : Time -> Delimiter -> List Delimiter -> Delimiter
nextDelimiter endTime delimiter delimiters =
    delimiters
        |> List.filter (\delim -> timeBefore delimiter.time delim.time)
        |> List.sortBy (\delim -> computeMinuteDiff delimiter.time delim.time)
        |> List.head
        |> Maybe.withDefault (endDelimiter endTime)


{-| Set the position of a moving delimiter
-}
moveDelimiter : Time -> Float -> MovingDelimiter -> MovingDelimiter
moveDelimiter startTime newY delimiter =
    if inMarginRange (timeToYPos startTime delimiter.from.time) (timeToYPos startTime delimiter.to.time) newY then
        { delimiter | delimiter = Delimiter delimiter.delimiter.id (yPosToTime startTime newY) }

    else
        delimiter


{-| Update a delimiter or add new one if the id is not present
-}
setDelimiter : List Delimiter -> Delimiter -> List Delimiter
setDelimiter delimiters newDelim =
    if List.any (\delim -> delim.id == newDelim.id) delimiters then
        List.map
            (\delim ->
                if delim.id == newDelim.id then
                    { delim | time = newDelim.time }

                else
                    delim
            )
            delimiters

    else
        newDelim :: delimiters


{-| View all delimiters
-}
viewDelimiters : Time -> Time -> List Delimiter -> List (Html Msg)
viewDelimiters timeStart timeEnd delimiters =
    delimiters
        |> List.filter (\delim -> inTimeRange ( timeStart, timeEnd ) delim.time)
        |> List.map (viewDelimiter timeStart)


viewDelimiter : Time -> Delimiter -> Html Msg
viewDelimiter timeStart delimiter =
    let
        minuteDiff =
            computeMinuteDiff timeStart delimiter.time
    in
    Html.div (Styles.delimiter minuteDiff) [ viewDelimiterLabel timeStart minuteDiff ]


viewDelimiterLabel : Time -> Int -> Html Msg
viewDelimiterLabel timeStart minuteDiff =
    Html.div Styles.delimiterLabel [ Html.text (timeToString <| minuteDiffToTime timeStart minuteDiff) ]


{-| View the moving delimiter
-}
viewMovingDelimiter : Time -> Maybe MovingDelimiter -> Html Msg
viewMovingDelimiter timeStart delimiter =
    delimiter
        |> Maybe.map (\movingD -> viewDelimiter timeStart movingD.delimiter)
        |> Maybe.withDefault (Html.text "")
