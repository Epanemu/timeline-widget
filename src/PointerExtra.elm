module PointerExtra exposing (relativePos)

import Common exposing (Coordinates)
import Html.Events.Extra.Pointer as Pointer


{-| Retrieve the relative position of a cursor on an element
-}
relativePos : Pointer.Event -> Coordinates
relativePos event =
    let
        pos =
            event.pointer.offsetPos
    in
    { x = Tuple.first pos, y = Tuple.second pos }
