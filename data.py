dataObject = """
{
  "startTime": {
    "hour": 8,
    "minute": 0
  },
  "endTime": {
    "hour": 0,
    "minute": 0
  },
  "tabs": [
    {
      "tabName": "Events",
      "blocks": [
        {
          "blockName": "some important thing",
          "blockTimes": [
            {
              "from": {
                "hour": 8,
                "minute": 0
              },
              "to": {
                "hour": 12,
                "minute": 0
              }
            },
            {
              "from": {
                "hour": 13,
                "minute": 30
              },
              "to": {
                "hour": 14,
                "minute": 0
              }
            }
          ]
        }
      ]
    },
    {
      "tabName": "Feelings",
      "blocks": []
    },
    {
      "tabName": "Productivity",
      "blocks": [
        {
          "blockName": "productive",
          "blockTimes": [
            {
              "from": {
                "hour": 8,
                "minute": 0
              },
              "to": {
                "hour": 12,
                "minute": 0
              }
            },
            {
              "from": {
                "hour": 13,
                "minute": 30
              },
              "to": {
                "hour": 14,
                "minute": 0
              }
            }
          ]
        },
        {
          "blockName": "unproductive",
          "blockTimes": [
            {
              "from": {
                "hour": 12,
                "minute": 0
              },
              "to": {
                "hour": 13,
                "minute": 0
              }
            }
          ]
        }
      ]
    }
  ]
}
"""

structureObject = """
{
  "mainTab": {
    "tabName": "Events",
    "canAdd": true,
    "defaultNames": [
      "default name1",
      "default name2"
    ]
  },
  "defaultStartTime": {
    "hour": 8,
    "minute": 0
  },
  "defaultEndTime": {
    "hour": 0,
    "minute": 0
  },
  "tabs": [
    {
      "tabName": "Feelings",
      "canAdd": true,
      "defaultNames": [
        "well",
        "fine",
        "terrible"
      ]
    },
    {
      "tabName": "Productivity",
      "canAdd": false,
      "defaultNames": [
        "productive",
        "unproductive",
        "contra-productive"
      ]
    }
  ]
}
"""