from flask import Flask, request, jsonify, Response
from data import dataObject, structureObject
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)
global dataVar
dataVar = dataObject

@app.route('/structure')
def handle_structure():
    if request.method=='GET':
       return structureObject

    else:
        return jsonify(error="Method not allowed")


@app.route('/data',methods=['GET','POST'])
def handle_data():
    global dataVar
    if request.method=='GET':
        resp = Response(dataVar)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = 'application/json'
        return resp

    elif request.method=='POST':
        dataVar = request.data.decode("UTF-8")
        return Response()

    else:
        return jsonify(error="Method not allowed")


if __name__ == "__main__":
    app.run()
